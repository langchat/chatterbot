FROM python

WORKDIR /code
COPY . .

RUN pip install -r ./requirements.txt