from flask import Flask, render_template, request
from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer
import json
import requests

app = Flask(__name__)

english_bot = ChatBot("Chatterbot", storage_adapter="chatterbot.storage.SQLStorageAdapter")

english_bot.set_trainer(ChatterBotCorpusTrainer)
#english_bot.train("chatterbot.corpus.english")

API_ENDPOINT = "http://localhost:3001"

USERNAME = 'bot'
PASSWORD = '123456'

r = requests.post(API_ENDPOINT + "/login", data={ 'email': USERNAME, 'password': PASSWORD })
auth = json.loads(r.text)['data']
print(auth)

@app.route("/", methods=['POST'])
def get_bot_response():
    data = json.loads(request.data)
    #return requestdata['content']
    userText = data['content']
    print("%s: %s" % (data['from'], data['content']))
    res = str(english_bot.get_response(userText))
    headers = { 'authorization': 'Token ' + auth['token'] }
    try: r = requests.post(API_ENDPOINT + "/thread/%s/message" % data['thread_id'],
                      data={ 'content': res }, headers=headers)
    except requests.HTTPError as e:
        print("Error")
    print("%s %s" % (USERNAME, res))
    return 'success'

if __name__ == "__main__":
    app.run()
